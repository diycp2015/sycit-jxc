## **三叶草CRM**

---

#### 版本号正式命名：三叶草CRM V1.0.0

###### 已于开始架构V2.0版本，可以说是推倒重构，紧随ThinkPHP5.1的新特性。欢迎有兴趣的小伙伴们一起交流。

###### 系统环境：PHP5.6 + Mysql5.7

###### 系统内核：ThinkPHP 5.0.11

###### 系统UI为：阿里云

###### 在线测试：www.sycit.cn (临时)

###### 官网QQ群：429455462

###### 数据文件：sycit-1123-01.sql 导入数据库即可，登录账户是：asdasd，密码一样。

###### 本系统虽然遵从 Apache-2.0开源许可协议，任何个人或组织可随意下载或做修改，但如商用请购买正式版权（包括遵从本系统的后期开发或任何代码的修改）。


---

###  以下是系统展示的图片


![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190545_0933e04c_490513.png "001.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190557_da11b17e_490513.png "002.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190607_2d9c6a85_490513.png "003.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190621_84ea831a_490513.png "004.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190633_d3940ffb_490513.png "005.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190644_6c914746_490513.png "006.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190655_5ffe572f_490513.png "008.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/190702_3e9dfe5b_490513.png "009.png")